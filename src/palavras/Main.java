package palavras;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[][] matriz = new int[][]{{-1,-1,-1,-1,-1},{-1,0,-1,-1,-1},{-1,0,-1,-1,-1},{-1,0,0,0,-1},{-1,-1,-1,-1,-1}};
		matriz = PalavrasCruzadas.numeraPalavras(matriz);
		int i, j;
		for(i=0;i<matriz.length;i++){
			for(j=0;j<matriz[0].length;j++){
				System.out.print(matriz[i][j]+" ");
			}
			System.out.println("\n");
		}
	}

}