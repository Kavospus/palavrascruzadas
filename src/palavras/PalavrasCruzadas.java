package palavras;

public class PalavrasCruzadas {
	
	public static int[][] numeraPalavras(int[][] matriz){
		int[][] result = matriz;
		int i,j,cont=1;
		for(i=1;i<matriz.length;i++){
			for(j=1;j<matriz[0].length;j++){
				if(i<matriz.length-2){
					if(matriz[i-1][j]==-1 && matriz[i][j] ==0 && matriz[i+1][j] == 0 && matriz[i+2][j] ==0){
						matriz[i][j]=cont;
						cont++;
					}
				}
				if(j<matriz[0].length-2){
					if(matriz[i][j-1]==-1 && matriz[i][j] ==0 && matriz[i][j+1] == 0 && matriz[i][j+2] ==0){
						matriz[i][j]=cont;
						cont++;
					}
				}
			}
		}
		result = matriz;
		return result;
	}

}
